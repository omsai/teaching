This is an shorter version of the BiocWorkshop
[102: Solving common bioinformatic challenges using GenomicRanges](https://bioconductor.github.io/BiocWorkshops/solving-common-bioinformatic-challenges-using-genomicranges.html#introduction-1) lesson.
It assumes knowledge of R from the previous 2 days of the workshop.

Time available: 1.5 hours

## Setup

- Packages required: `airway`, `AnnotationHub`

## Exercises

None :(

## Post workshop feedback

- One or two learners lacked a conceptual understanding of genomic
  coordinates and genome browsers. One must illustrate the concept at
  the beginning of the lesson.  Using drawings for Genomic operations
  was less tedious, but still needed a fair amount of explanation.
- Would have really helped to have a cheat-sheet of GenomicRanges
  operations like bedtools for descriptions of some functions on their
  website.
- A worksheet of problems showing the desired graphical result and
  asking for code operations to get that result.  Could craft such a
  worksheet to involve group exercises.
- Need more biologically relevant names than `gr`, `g`, `g2`, `g3` to
  help drive home **why** we are doing certain genomic operations.
- Failed to download the AnnotationHub BigWig file for section
  [4.17.1](https://bioconductor.github.io/BiocWorkshops/solving-common-bioinformatic-challenges-using-genomicranges.html#extracting-data-from-annotationhub).
  The download stalled for several learners over the lunch break.  Not
  sure if JAX wireless issue or bioconductor server load issue.
- Questions for the [4.16
  exercises](https://bioconductor.github.io/BiocWorkshops/solving-common-bioinformatic-challenges-using-genomicranges.html#exercises)
  make no sense.  They don't relate to the rest of the lesson.
- Might help to reduce talking about the many possible GenomicRanges
  operations and instead talk about an example problem that flows
  through all of the lessons.
- Jethro suggested changing the lesson order: teaching GenomicRanges
  earlier on in the day after the Bioconductor introduction, and
  keeping the DESeq2 and Annotation lessons together.
