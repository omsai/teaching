This is an expanded take on the BiocWorkshop
[100: R and Bioconductor for everyone](https://bioconductor.github.io/BiocWorkshops/r-and-bioconductor-for-everyone-an-introduction.html#introduction-to-bioconductor) lesson.
It assumes knowledge of R from the previous 2 days of the workshops
and starts from the Bioconductor section onwards.

Time available: 1 hour

## Advice from teaching at Bar Harbor the week before

### Asli's feedback

- Setup directory structure to avoid finding where things are.
- People wanted more detail.
  - Library size normalization - maybe carefully read through Vignettes
    beforehand?
- Function `all_data` is assigned to object `all_data`.  Can be confusing.
- Didn't have time for `ReportingTools`.

### Dan's feedback

- Slow down!
- Leave real-estate on the screen open for your RStudio history tab.

## Setup

- Create or open a "bioconductor" project in RStudio - top-right tab
  should say "bioconductor".  Learners appearing only for the
  Bioconductor portion of the workshops would not have their RStudio
  project setup.  Create the `~/Desktop/bioconductor` RStudio project
  following [Sue's
  instructions](https://smcclatchy.github.io/2018-10-12-bioconductor-bh/#packages)
- Packages required:
  `rtracklayer`,
  `TxDb.Hsapiens.UCSC.hg38.knownGene` and
  `DESeq2`

- Files required:
  [`CpGislands.Hsapiens.hg38.UCSC.bed`](https://github.com/Bioconductor/BiocWorkshops/raw/master/100_Morgan_RBiocForAll/CpGislands.Hsapiens.hg38.UCSC.bed),
  [`airway_counts.csv`](https://github.com/Bioconductor/BiocWorkshops/raw/master/100_Morgan_RBiocForAll/airway_counts.csv)
  and
  [`airway_colData.csv`](https://github.com/Bioconductor/BiocWorkshops/raw/master/100_Morgan_RBiocForAll/airway_colData.csv)

## Motivation

- Convenience and Power:
    - Installing software inside of R instead of all over the computer.
    - Works everywhere: all operating systems, HPC cluster.  Doesn't
      require use of HPC cluster and even works on Windows which are
      about half of Bioconductor users per 2017 website visitor
      statistics.  Doesn't require admin permissions on the HPC
      cluster because one only installs packages in home R directory
      to get working environment.
    - High quality packages. A lot of newer work requires you to use
      Bioconductor.
    - For genomics work, closely integrated unlike python ecosystem.
    - Easy to read, way more reproducible; intended to replace all
      your shell scripts downstream of your having your aligned
      `*.bam` sequence files.
    - Speed: excellent vectorization; BiocParallel, etc. Speed ease of
      writing scripts.
- Community:
    - Support site is responsive and developers answer questions.

## Exercises

None :(

## Learning goals

- Find, install, and learn how to use Bioconductor packages.
- Import and manipulate genomic files and Bioconductor data objects.
- Start an RNA-seq differential expression workflow.

## Learning objectives

- Discover, install, and read the vignette of the `DESeq2` package.
- Discover the 'single cell sequencing' vignette.
- Import BED and GTF files into Bioconductor.
- Find regions of overlap between the BED and GTF files.
- Import a matrix and data.frame into Bioconductor's `SummarizedExperiment`
  object for RNA-Seq differential expression.

## Post workshop feedback

- Half the audience had heard of Bioconductor or used it to some
  extent before the workshop.  Only 2 users use it on HPC; not much of
  a use case.
- One learner really liked `keepStandardChromosomes`.
- Checking `BiocManager::valid()` returned `FALSE` for many because of
  **non-Bionductor** packages being out-of-date but nevertheless was
  disruptive.
- Using `library(BiocManager)` followed by `install(...)` created
  confusion because some learners forgot to run the library command
  and instead auto-completed `install.packages(...)`.  Using
  `BiocManager::install(...)` would have been better.
- When showing
  [https://bioconductor.org/packages](https://bioconductor.org/packages),
  I accidentally had the bottom "Workflows" package type selected
  while clicking through the package categories, and therefore my
  package searches failed.  My error was later pointed out by a
  learner.
- Was not able to get through `SummarizedExperiment`.  Introduced SE
  diagram, but failed to create SE object due to mismatching
  dimensions of `counts` and `colData`.
- Learner questions:
    - Confusion about why we show the histogram of CpG widths.
      Perhaps the issue was not giving enough background about what
      CpG islands for a computer programmer.
	- seqinfo: why is there a "circular" chromosome in the human
      genome?  Turns out it was mitochondrial by inspecting
      `seqinfo(tx)`.
    - `table(olaps)` was also confusing.
    - One learner pointed out that `countOverlaps(tx, cpg)` is a crude
      test for CpG islands overlapping genes.
    - Reading airway CSVs with `row.names = 1` was also confusing.
      Maybe because `rownames()` was not familiar.
