The first 45 minutes is a shortened take on Software Carpentry's
[UNIX shell lesson](https://swcarpentry.github.io/shell-novice/)

[HPC Carpentry](https://hpc-carpentry.github.io/) is also a useful
resource.

## Cheatsheets

- [HPC Handout](attachments/hpc/hpc-handout.pdf)
- [Wiki](https://wiki.hpc.uconn.edu)

## Lessons

Time        | Segment
------------|--------------
00:00-00:10 | Setup - Cluster login
00:10-00:15 | HPC Overview (Jon)
00:15-00:45 | Shell - Navigating files and directories
00:45-01:25 | Scheduler - Interactive GPU jobs with srun
01:25-01:30 | Scheduler - Batch GPU jobs with sbatch
01:30-01:35 | Data - Transferring files
01:35-02:00 | General Discussion
