The first hour is a shortened take on Software Carpentry's
[UNIX shell lesson](https://swcarpentry.github.io/shell-novice/)

Although not explored, [HPC Carpentry](https://hpc-carpentry.github.io/) is
also potentially useful.

## Cheatsheets

- [HPC Handout](attachments/hpc/hpc-handout.pdf)
- [Wiki](https://wiki.hpc.uconn.edu)

## Lessons

Time  | Segment
------|--------------
00:00 | Setup - Cluster login
00:15 | Shell - Navigating files and directories
00:45 | Shell - Working with files and directories
01:15 | Modules - loading software
01:20 | Scheduler - SLURM submitting jobs
01:45 | Demo - Transferring files
01:55 | Demo - Emacs ESS
