Adapted from [R for Data Science](https://r4ds.had.co.nz) chapters:

- [Relational data](https://r4ds.had.co.nz/relational-data.html)
- [Strings](https://r4ds.had.co.nz/strings.html)
- [Factors](https://r4ds.had.co.nz/factors.html)
- [Dates and times](https://r4ds.had.co.nz/dates-and-times.html)
- Modeling
    - [Model intro](https://r4ds.had.co.nz/model-intro.html)
    - [Model basics](https://r4ds.had.co.nz/model-basics.html)
    - [Model building](https://r4ds.had.co.nz/model-building.html)

Chapter         | Time available (hours) | Time table
----------------|------------------------|-------------------
Tibbles         | 1.10                   | 2020-08-03, 10:15am - 11:25am
Relational data | 1.25                   | 2019-05-29, 9:00am - 10:15am
Strings         | 2.00                   | 2019-05-29, 10:30am - 12:30pm
Factors         | 1.25                   | 2019-06-05, 9:00am - 10:15am
Dates and times | 0.50                   | 2019-06-05, 10:15am - 10:45am
Modeling        | 1.50                   | 2019-06-12, 11:00am - 12:30am

## Setup

- Update RStudio.
- Increase RStudio zoom to 175% from: Tools > Global Options >
  Appearance > Zoom > 175%
- No files to download; all data is included in the package list.

## Teaching

- Close out any existing R project and create a new project.
    - Emphasize the usefulness of projects: saving data, directory
      consistency, files opened, etc.
- Install packages for the tibbles lesson
```R
pkgs <- c("tidyverse", "nycflights13")
to_install <- setdiff(pkgs, installed.packages()[, 1])
if (length(to_install)) install.packages(to_install)
```

- Install packages for the relational data and strings lessons
  
```R
pkgs <- c("tidyverse", "nycflights13", "fueleconomy", "nasaweather", "babynames", "maps", "htmlwidgets")
to_install <- setdiff(pkgs, installed.packages()[, 1])
if (length(to_install)) install.packages(to_install)
```

- Install packages for the factors, dates and times lessons

```R
pkgs <- c("tidyverse", "nycflights13")
to_install <- setdiff(pkgs, installed.packages()[, 1])
if (length(to_install)) install.packages(to_install)
```

- Install packages for modeling

```R
pkgs <- c("tidyverse", "nycflights13", "hexbin")
to_install <- setdiff(pkgs, installed.packages()[, 1])
if (length(to_install)) install.packages(to_install)
```

- Create a new R script and immediately save it so that the syntax
  highlighting and tool-tip help system in RStudio is enabled for the
  script.
  - Share script using `tmate` web link showing `emacs` with `auto-revert-mode`
    enabled.
  - The lecture loosely based off the textbook is here:
    - Tibbles:
      [preparation](https://gitlab.com/omsai/teaching/tree/master/code/r4ds/10_tibbles.R)
    - Relational data:
	  [preparation](https://gitlab.com/omsai/teaching/tree/master/code/r4ds/13_relational_data.R),
      [transcript](https://gitlab.com/omsai/teaching/blob/master/transcripts/20190529T0900-r4ds-relational_data.R)
	- Strings:
	  [preparation](https://gitlab.com/omsai/teaching/tree/master/code/r4ds/14_strings_regex.R),
	  [transcript](https://gitlab.com/omsai/teaching/blob/master/transcripts/20190529T1030-r4ds-strings_and_regex.R)
	- Factors:
	  [preparation](https://gitlab.com/omsai/teaching/tree/master/code/r4ds/15_factors.R)
	- Dates and times:
	  [preparation](https://gitlab.com/omsai/teaching/tree/master/code/r4ds/16_dates_and_times.R)
	- Modeling:
	  [preparation](https://gitlab.com/omsai/teaching/tree/master/code/r4ds/22-24_modeling.R)
- Compared to the textbook, the intended lectures:
    - Remove code that would have taken too long or are not very
      useful.
    - One can comfortably cover about 70 lines per hour.  These
      intended lecture scripts are twice as long, so one will have to
      drop lower priority sections or commands that are not as useful.
    - Not all challenges are introductory level, and in some places
      the text has been adapted as fill-in-the blank exercises.  For
      example [Challenge 4 of
      stringr](https://gitlab.com/omsai/teaching/tree/master/code/r4ds/14_strings_regex.R#L71).
    - Cover at least 3 challenges per hour.

### Tibbles

- Goal is to cover subtle fundamentals of tibbes for subsequent lessons this
  week of data import, tidy data, and next week for relational data.
- Should be a relatively slow lesson and pick up the next few days.  I'm not
  going to have you rely very much at all on the [previous
  workshop](https://smcclatchy.github.io/2020-07-14-r4ds-online/) topics:
    1. Introduction to data exploration
    1. Data visualization
    1. Workflow: basics
    1. Data transformation
    1. Workflow: scripts
    1. Exploratory data analysis
    1. Workflow: projects
- Show the classic "data science explore" image from the introduction.
- Mention we are creating a project for the key reasons from last lesson.

### Relational data

- Mention that the this builds on the dplyr and tidyr lessons from last week.
    - Columns are variables and rows are observations.
    - `select()` subsets columns and `filter()` subsets rows.
- Refer often to the figures from the [lesson
  page](https://r4ds.had.co.nz/relational-data.html).  Zoom into the
  page; no need to create a presentation with the figures separately.

### Strings

- Point to the cheatsheets and refer back to them often:
    - [regex - MIT](http://web.mit.edu/hackl/www/lab/turkshop/slides/regex-cheatsheet.pdf)
	- [regex - stringr](https://github.com/rstudio/cheatsheets/raw/main/strings.pdf)
	- [data transformation](https://github.com/rstudio/cheatsheets/raw/main/data-transformation.pdf)

### Factors

- Mention this lesson requires some knowledge of `ggplot2` and `dplyr`
  and point to cheatsheets:
    - [ggplot2 - visualization](https://github.com/rstudio/cheatsheets/raw/main/data-visualization-2.1.pdf)
    - [dplyr - data transformation](https://github.com/rstudio/cheatsheets/raw/main/data-transformation.pdf)
- Also there is a `forcats` cheatsheet:
    - [factors - forcats](https://github.com/rstudio/cheatsheets/raw/main/factors.pdf)

### Dates and times

- There is a `lubridate` cheatsheet:
    - [lubridate](https://github.com/rstudio/cheatsheets/raw/main/lubridate.pdf)
- Skip most of the plotting because it involves too much typing.

### Modeling

Using my
[transcripts](https://gitlab.com/omsai/teaching/blob/master/transcripts/)
from teaching the previous lessons and knowing how much time I took to
cover them, I [created a
model](https://gitlab.com/omsai/teaching/blob/master/transcripts/timeit.R)
to estimate how much material I can pack into this lesson.  For the 90
minutes I have, I estimate that I can cover ~80 lines of code and 2
challenges or ~2,500 characters, but the actual planned transcript is
twice that much.  To work around covering twice as much code as the
students can comfortably type, the students will be instructed to type
along until they complete the first challenge, and then they have the
option to watch instead of typing to build their intuition.  The
second challenge has all the setup code they need.

Following the textbook introduction to modeling linearly would be a
lot of talking and will probably not be engaging.  It would make more
sense to dive into coding and visualization, and explain the concepts
along the way.

- Introduction:
    - Predictive vs data-discovery models.  Also called supervised vs
      unsupervised.
    - Not going to discuss mathematical theory.
    - You will get an intuition about how statistical models work.
	- Hypothesis generation vs confirmation
	    - Data can be used for observation or confirmation but not
          both.
        - Observe as many times, confirm only once.
	- 60-20-20 training-query-test data split.
        - Training: do anything you like.
        - Query: compare models or visualizations by hand but do not
          automate.
        - Test: Only use once to test your final model.
    - Data Split does not apply to Bayesian modeling which instead
      uses priors.
- Models provide a low-dimensional summary of the dataset.
- Partition data into patters and residuals.  Peel back layers of
  structure in a dataset.
- A model family is an equation.
    - Variables vs parameters.
	- Vary parameters to capture patterns.
 - Fit the model by setting those parameters to be close to your data.
 - Best model among family doesn't imply it is a good model or that
   the model is "true".
    - "All models are wrong, but some are useful." - George Box
- Goal is not to uncover truth but to find a simple, useful
  approximation.

## Feedback

### Tibbles

What is one thing you learned that you found useful?

- tibble..?
- Recognizing the differences between data frames and tibbles and knowing the
  advantages of tibbles is very useful
- I have never understood tibbles, now I have a better understanding.
- learning about tibbles
- I didn't know about tibbles at all so it was all new to me and I also used
  pipes only once so it was nice to relearn and refresh my memory!
- tibbles
- The way that we organize the R scripts
- Tidyverse's tibbles are better and more efficient to work with than base R
  data.frames.

What is one question you still have about the material?

- What is the rationale to use "tibble" instead of using a regular R data frame
  work
- Do I always want to turn a .CSV into a tibble to work with it?
- Not sure if I'm convinced to switch from dataframes to tibbles. Don't seem to
  be a lot of benefits from what was shown.
- none currently
- is sub setting tibbles returning vectors?
- How to use tibble function
- Should I first read into RStudio a spreadsheet data set with `read_csv()`,
  then coerce to a tibble with `as_tibble()`?  Is this the standard order of
  operations?

Is there anything else you would like to add?

- Pariksheet was very clear in his teaching, and went at a perfect pace. His
  simple but informative notes in R studio were very helpful and easy to copy
  down.
- Examples... Starting each section with "say you wanted to..."
- This is really helpful and great!! I can't thank you enough for providing
  this workshop and I and I am excited for the upcoming ones. the tutors are
  great at teaching.
- Nothing
- Pariksheet is a very helpful instructor!  He communicates the material
  clearly, logically, and the pace is not too fast.

### Relational data, strings and regex

What is the most important thing you learned this morning?

- how to join dataframes +1
- The different ways to join dataframes, and backreferencing
- how to find specific characters in strings and making outline on R
- how to explore and view strings

Write down one thing that is confusing and you'd like clarification on.

- strings and regex
    - Getting acquainted with regex takes a fair amount of experience.
      There are a lot of rules we saw this afternoon.  The way to get
      good at regex is to apply it to lots of problems.  Like R, using
      regex is powerful, in that it can do a lot of work with very
      little code.
- how do you create "if/else" statements to circumnavigate specific
  functions that you can't remember the name of
    - If you don't remember the name of a function use two question
      marks and any word relating to the function.  For example if I
      want to find all functions relating to splitting, type `??split`
      at the R interpreter shell.
- when the different ways to join dataframes are useful
    - You'll use `left_join` most often.  `full_join` if you need to
      keep non-matching records from the right dataframe.

### Factors, dates and times, functions and pipes

What is the most important thing you learned this morning?

- The T pipe operator can send two outputs out
- Manipulating NA in ggplot/ graphs, the different pipe functions
- Use pipes to avoid repeating and some possible mistakes

Write down one thing that is confusing and you’d like clarification on.

- Some functions, like `cor()`, don't accept data frames. Is there a
  way to predict which functions do this? Can you tell by which
  packages they are from?
    - Actually most functions don't work on data frames natively,
      except `dplyr` and `tidyr`.  Usually, to apply a function on a
      dataframe I would run them inside of `mutate()` or `summary()`,
      etc.  Or if I wanted to do harder things with a dataframe,
      there's also `do()`.  There's a good chapter on non-standard
      evaluation that covers some of these concepts.

### Overall course feedback

Write one thing you really like about this course.

- Regular expressions
- It was great - it taught me about some of the basics that I had struggled to
  understand.  The speed has been good - not too fast
- I really liked learning about creating and customizing plots
- The instructors were great.  The course content, i.e. R4DS book, is also
  excellent

Write one suggestion to improve this course.

- More sessions covering more topics!!!!  I would have really benefited from
  another couple of session or perhaps deeper dives into some of the topics
- I would love to learn more specifics about summary statistics within plots
  and adding details such as sample size.  I think it would be helpful if
  participants could access a copy of the instructors code after each class in
  case there's anything they missed/couldn't keep up with
- Make it standard practice to share script/code with all participants at the
  end of each lesson.  This will reduce focus from frantically typing code to
  keep up with instructor to a focus on the lesson content itself

Is there anything else you would like to add?

- Thank you so much for putting on this course and making it available for
  free.  You have really helped my R knowledge
- Thank you!!
- Thank you so much for making this course available!  It's invaluable
