This is a shorted and modern take on
[R for Reproducible Scientific Analysis](http://swcarpentry.github.io/swc-releases/2016.06/r-novice-gapminder/) lesson

Time available: 10:30am - 2:30pm = 3.5 hours with 0.5 hour lunch.

## Teaching goals for data analysis

- Importing and manipulating tabular data.
- Plotting.
- Understanding data types.
- Modern R tidyverse functions.

## Lessons:

- Familiarity with RStudio, especially projects (0.25 h)
- Data structures (1.5 h)
- tidyverse (1 h)
    - long-short form
    - split-apply-combine (if there's time)
- ggplot (0.75 h)

## Cheat Sheets

Grab from https://www.rstudio.com/resources/cheatsheets/

- Data structures: [Base R Cheat Sheet](http://github.com/rstudio/cheatsheets/raw/main/base-r.pdf)
- dplyr: [Data Transformation Cheat Sheet](https://github.com/rstudio/cheatsheets/raw/main/data-transformation.pdf)
- tidyr: [Data Import Cheat Sheet](https://github.com/rstudio/cheatsheets/raw/main/data-import.pdf)
- ggplot: [Data Visualization Cheat Sheet](https://github.com/rstudio/cheatsheets/raw/main/data-visualization-2.1.pdf)

For some reason my HP printer ignores the long / short edge flip and
my workaround is to rotate odd pages by 180 degrees:

```bash
#!/bin/bash
# Rotate odd pages for double-sided printing using pdf tools.
if [[ -z "$1" ]]; then
    echo >&2 "Missing input PDF file"
    exit 1
fi
input=$1
dir=$(dirname ${input})
input=$(basename ${input})
stem=${input%.pdf}
suffix=rotated180
(
    pushd /tmp
    pdfseparate ${dir}/${input} ${stem}-%d.pdf
    pdf180 ${stem}-2.pdf
    pdfjoin ${stem}-1.pdf ${stem}-2-${suffix}.pdf --outfile ${dir}/${stem}-${suffix}.pdf
    rm ${stem}-1.pdf ${stem}-2.pdf ${stem}-2-${suffix}.pdf
    popd
)
```

## Exercises

- 2 per hour?  7 total.  Some might work for Socrative.
  Won't need Socrative for only 2 people.

## Motivation

- Programming in a nutshell.
    - Hope this will be a long journey.
    - Worst thing in life is to be jaded.
- Learning programming requires:
    - Giving yourself time to analyze.
    - Good help:
        - Stackoverflow
		- People: don't allow anyone else to use your keyboard

## Commands

Show [messy project](https://swcarpentry.github.io/swc-releases/2016.06/r-novice-gapminder/fig/bad_layout.png).

We will maintain the following project structure:

```text
.
├── data/      - raw data only
├── results/   - modified data, spreadsheets, plots
└── scripts/   - computer programs

3 directories, 0 files
```

Create the `data/feline-data.csv` file
[as described](http://swcarpentry.github.io/swc-releases/2016.06/r-novice-gapminder/04-data-structures-part1/),
but replace 1 with T and 0 with F in the last column.

Create an R script in [`scripts/data_structures.R`](https://gitlab.com/omsai/teaching/blob/master/code/r-in-4-hours/scripts/data_structures.R)
