Main lesson: [https://omsai.github.io/stat-5361/git-lesson.html](https://omsai.github.io/stat-5361/git-lesson.html)

## Setup

Inside of RStudio open a terminal and run these 2 commands.

```
git --version
# git version 2.__._
ssh -T git@github.com
# Hi _____! You've successfully authenticated, but GitHub does not provide shell access.
```

If the first command complains of not finding git, install git from the
workshop page.

If the second command does not give you that message, make sure you registered
on GitHub.com and added your SSH key.


## Additional resources

- Hadley Wickham's book chapter: [RStudio Git and GitHub](http://r-pkgs.had.co.nz/git.html)
- Online book: [Happy Git with R](https://happygitwithr.com/)
- Video: [RStudio and GitHub](https://rstudio.com/resources/webinars/managing-part-2-github-and-rstudio/)
