# Overview

Hi there!

I've been teaching [Carpentry](https://carpentries.org/) workshops since 2015
and here you can find web my preparation, classroom transcripts,
and instruction notes.

In addition to setup and teaching materials from before the class,
I also include transcripts
of my typed code, terminal sessions, and shared note taking from the classroom
to help with
[post-workshop instructor debriefings](https://docs.carpentries.org/topic_folders/hosts_instructors/hosts_instructors_checklist.html#instructor-checklist)
and to inform future workshops.

There will be some variation among the classroom transcripts because,
as with any teaching,
lessons get adapted to to the learners during teaching, as well as
host requests and pre-workshop instructor decisions.

In summary, the resources I provide are:

- Scripts to [post your shell history](Shell-history.md) with low latency.
  That way, learners can catch up on any commands they have missed,
- [Socrative quiz](#carpentry-workshops) import codes,
- Instructor notes,
- Instructor [shell history](#carpentry-workshops), and finally
- Shared [student notes](#carpentry-workshops)

You can find the newest setup and instructor notes in the menus,
and everything else in the tables below.

I find it easier to teach with
printed lesson materials and instruction notes
instead of futzing around with a phone or tablet.

For you who are about to teach,
I'll leave you with the same benediction
on Greg Wilson's book cover of
"How to Teach Programming (and Other Things)":

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Do not worry and you will not make mistakes.

&#9829;
Pariksheet

## Carpentry workshops

Date         | Topic                       | Location                  | Resources
------------:|-----------------------------|---------------------------|----------
Mar 21, 2024 | Python                      | University of Michigan    | [Student notes](https://pad.carpentries.org/2024-03-21-umich),<br />[Workshop](https://umcarpentries.org/2024-03-21-UMich-python/)
Dec  1, 2023 | Python                      | University of Michigan    | [Teaching notes data analysis](https://gitlab.com/omsai/teaching/blob/master/transcripts/20231130-python-data-analysis.ipynb),<br />[Transcript data analysis](https://gitlab.com/omsai/teaching/blob/master/transcripts/20231201-python-data-analysis.ipynb),<br />[Student notes](https://pad.carpentries.org/2023-12-01-umich),<br />[Workshop](https://umcarpentries.org/2023-12-01-UMich-python-pilot/),<br />[Debrief notes](https://pad.carpentries.org/2023-12-umich)
May 24, 2023 | Git                         | University of Michigan    | [Workshop](https://umcarpentries.org/2023-05-24-UMich/)
Dec  8, 2022 | R for Data Analysis         | University of Michigan    | [Socrative SOC-69557105](https://b.socrative.com/teacher/#import-quiz/69557105),<br />[Workshop](https://umcarpentries.org/2022-12-08-UMich-online/)
Oct 21, 2022 | N/A - Helper                | University of Michigan    | [Workshop](https://umcarpentries.org/2022-10-20-UMich/)
Feb 24, 2020 | Git                         | University of Connecticut | [Worksheet](attachments/git/git-worksheet.pdf),<br />[Worksheet source](attachments/git/git-worksheet.tex)<br />[Teaching notes](https://uw-madison-datascience.github.io/git-novice-custom/),<br />[Workshop](https://carpentries-uconn.github.io/2020-02-24-uconn/)
Jan 14, 2020 | Git                         | University of Connecticut | [Worksheet](attachments/git/git-worksheet.pdf),<br />[Worksheet source](attachments/git/git-worksheet.tex)<br />[Teaching notes](https://uw-madison-datascience.github.io/git-novice-custom/),<br />[Workshop](https://carpentries-uconn.github.io/2020-01-13-uconn/)
Jan 13, 2020 | Shell, HPC                  | University of Connecticut | [Socrative SOC-43992085](https://b.socrative.com/teacher/#import-quiz/43992085),<br/>[Parallel example 3](https://github.uconn.edu/HPC/parallel-slurm),<br />[Workshop](https://carpentries-uconn.github.io/2020-01-13-uconn/)
Oct 11, 2019 | Git in RStudio              | University of Connecticut | [Teaching notes](https://omsai.github.io/stat-5361/git-lesson.html),<br />[Workshop](https://carpentries-uconn.github.io/2019-10-10-uconn/)
Aug 29, 2019 | Git in RStudio              | University of Connecticut | [Teaching notes](https://omsai.github.io/stat-5361/git-lesson.html),<br />[Workshop](https://omsai.github.io/stat-5361/git-setup.html)
Jun 13, 2019 | Python                      | Regeneron                 | [Workshop](https://mckays630.github.io/2019-06-13-python-regn/)
Jan 31, 2019 | Python                      | Jackson Labs              | [Socrative SOC-38419560](https://b.socrative.com/teacher/#import-quiz/38419560),<br />[Notebook history](https://colab.research.google.com/drive/1VjEdhvBA5CkB0eXo5Sqg9NcpEHqbwIpa#scrollTo=NFmWwLApaiF-),<br />[Student Notes](https://pad.carpentries.org/2019-01-30-python-ct),<br />[Workshop](https://thejacksonlaboratory.github.io/2019-01-30-python-ct/)
Nov 30, 2018 | Git                         | University of Connecticut | [Worksheet](attachments/git/git-worksheet.pdf),<br />[Worksheet source](attachments/git/git-worksheet.tex)
Aug 28, 2018 | R for Reproducible Research | University of Connecticut | [Teaching notes](R-in-4-hours.md)
Jul 25, 2018 | Git                         | Jackson Labs              | [Shell history](attachments/git/swc-2018-07-25.html),<br />[uploader.py](Shell-history.md#uploaderpy),<br />[Socrative SOC-34475784](https://b.socrative.com/teacher/#import-quiz/34475784),<br />[Student notes](http://pad.software-carpentry.org/2018-07-24-jacksonLab),<br />[Workshop](https://smcclatchy.github.io/2018-07-24-jacksonLab/)
May 10, 2018 | Git                         | University of Connecticut | [Blog post](https://software-carpentry.org/blog/2018/05/git-worksheets.html),<br />[Student notes](https://pad.carpentries.org/2018-05-10-UCONN),<br />[Workshop](https://tem11010.github.io/2018-05-10-UConn/)
Apr  2, 2018 | R for Reproducible Research | Jackson Labs              | [Student notes](https://public.etherpad-mozilla.org/p/2018-04-02-r-farmington),<br />[Workshop](https://smcclatchy.github.io/2018-04-02-r-farmington/)
Nov 15, 2017 | R for Reproducible Research | Jackson Labs              | [Student notes](https://public.etherpad-mozilla.org/p/2017-11-15-r-ct),<br />[Workshop](https://smcclatchy.github.io/2017-11-15-jackson/)
Apr  6, 2017 | Python (Data Carpentry)     | Jackson Labs              | [Student notes](https://pad.carpentries.org/2017-04-06-farmington),<br />[Workshop](https://smcclatchy.github.io/2017-04-06-farmington/)
Mar 29, 2017 | Genomics (Data Carpentry)   | University of Delaware    | [Socrative SOC-27711526](https://b.socrative.com/teacher/#import-quiz/27711526),<br />[Student notes](http://pad.software-carpentry.org/2017-03-29-udel),<br />[Workshop](https://omsai.github.io/2017-03-29-udel/)
Jan 13, 2017 | Git                         | University of Connecticut | [Socrative SOC-26214965](https://b.socrative.com/teacher/#import-quiz/26214965),<br />[Student notes](https://pad.carpentries.org/2017-01-12-UCONN),<br />[Workshop](https://mickley.github.io/2017-01-12-UCONN/)
Oct  6, 2016 | N/A - Helper                | Jackson Labs              | [Student notes](https://pad.carpentries.org/2016-10-06-jackson),<br />[Workshop](https://smcclatchy.github.io/2016-10-06-jackson/)
Jun 17, 2015 | Python and Git              | University of Connecticut | [Workshop](https://mckays630.github.io/2015-06-17-ucstorrs/)
Jun 15, 2015 | N/A - Helper                | University of Connecticut | [Workshop](https://iglpdc.github.io/2015-06-15-uconn/)

## Bioconductor workshops

Date         | Topic                           | Location                  | Resources
------------:|---------------------------------|---------------------------|----------
Apr 19, 2024 | Bioconductor overview           | University of Michigan    |
Jan 16, 2019 | Bioconductor full day           | University of Connecticut | [Workshop](https://omsai.github.io/2019-01-14-uconn/)
Oct 17, 2018 | Bioconductor full day           | Jackson Labs              | [Teaching notes for Intro](Bioc-intro.md),<br />[Teaching notes for GRanges](Bioc-GRanges.md),<br />[Student notes](https://pad.carpentries.org/2018-10-17-bioconductor-ct),<br />[Workshop](https://smcclatchy.github.io/2018-10-17-bioconductor-ct/)
Jun 28, 2018 | Gviz metagene and headmap plots | University of Connecticut | [Vignette](https://gitlab.com/coregenomics/gviztut/blob/master/vignettes/metagene.Rmd)
Sep 21, 2017 | GenomicRanges, GAlignments, etc | University of Connecticut | [Vignette](https://github.com/coregenomics/RangesTutorial2017/blob/master/vignettes/granges-intro.Rmd)

## HPC workshops

Date         | Topic                           | Location                  | Resources
------------:|---------------------------------|---------------------------|----------
Jul 10, 2020 | Autotools build system          | University of Connecticut | [Transcript](https://gitlab.com/omsai/teaching/blob/master/transcripts/20200710T1300-hpc-autotools.txt)
Jan 16, 2020 | HPC and GPUs for TensorFlow     | University of Connecticut | [Teaching notes](HPC-TensorFlow.md)
Nov 22, 2019 | HPC for Math in 1 hour          | University of Connecticut | [Handout](attachments/hpc/hpc-handout.pdf),<br />[Handout source](attachments/hpc/hpc-handout.tex)
Jul  2, 2019 | HPC for C/C++ and threading     | University of Connecticut | [Git repo](https://github.uconn.edu/HPC/parallel-intro-cpp)
Mar  8, 2019 | HPC R                           | University of Connecticut | [Presentation and Handout](https://omsai.gitlab.io/rhpc)
Nov  2, 2018 | HPC Essentials                  | University of Connecticut | SOC-26255463,<br />[Handout](attachments/hpc/hpc-handout.pdf),<br />[Handout source](attachments/hpc/hpc-handout.tex)
Apr 17, 2017 | HPC Essentials                  | University of Connecticut | SOC-26255463,<br />[Workshop](https://github.uconn.edu/pages/HPC-workshops/2017-04-17_hpc-intro/)
Mar 20, 2017 | HPC Intermediate                | University of Connecticut | SOC-27589085,<br />[Teaching notes](https://wiki.hpc.uconn.edu/index.php/HPC_Intermediate),<br />[IPython notes](https://wiki.hpc.uconn.edu/index.php/HPC_Intermediate_Python)
Mar  6, 2017 | HPC Essentials                  | University of Connecticut | [Teaching notes](https://wiki.hpc.uconn.edu/index.php/HPC_Intro)
Jan 17, 2017 | HPC Computational Chemistry     | University of Connecticut | (same as above)
Feb  2, 2016 | HPC Data Mining                 | University of Connecticut | (same as above)

## R for data science workshops

Date         | Topic                           | Location                  | Resources
------------:|---------------------------------|---------------------------|----------
Aug 12, 2020 | Relational data, Factors, Strings | Online                  | [Transcript](https://gitlab.com/omsai/teaching/blob/master/transcripts/20200812T1015-r4ds-relational_data-factors-strings_and_regex.R),<br />[Teaching notes](R-for-Data-Science.md),<br />[Student notes](https://pad.carpentries.org/OGhcx80UWGOwdWtaqn4E),<br />[Workshop](https://smcclatchy.github.io/2020-08-03-r4dsWrangle/)
Aug  3, 2020 | Tibbles                         | Online                    | [Transcript](https://gitlab.com/omsai/teaching/blob/master/transcripts/20200803T1015-r4ds-tibbles.R),<br />[Teaching notes](R-for-Data-Science.md),<br />[Student notes](https://pad.carpentries.org/OGhcx80UWGOwdWtaqn4E),<br />[Workshop](https://smcclatchy.github.io/2020-08-03-r4dsWrangle/)
Jun 12, 2019 | Modeling                        | UCHC                      | [Teaching notes](R-for-Data-Science.md),<br />[Student notes](https://pad.carpentries.org/2019-05-01-r-for-data-science-ct),<br />[Workshop](https://smcclatchy.github.io/2019-05-01-r-for-data-science-ct/)
Jun  5, 2019 | Factors, Dates and times        | UCHC                      | [Transcript factors](https://gitlab.com/omsai/teaching/blob/master/transcripts/20190605T0900-r4ds-factors.R),<br />[Transcript dates times](https://gitlab.com/omsai/teaching/blob/master/transcripts/20190605T1030-r4ds-dates_times.R),<br />[Transcript pipes_functions](https://gitlab.com/omsai/teaching/blob/master/transcripts/20190605T1100-r4ds-pipes_functions.R),<br />[Teaching notes](R-for-Data-Science.md),<br />[Student notes](https://pad.carpentries.org/2019-05-01-r-for-data-science-ct),<br />[Workshop](https://smcclatchy.github.io/2019-05-01-r-for-data-science-ct/)
May 29, 2019 | Relational data, Strings        | UCHC                      | [Transcript relational data](https://gitlab.com/omsai/teaching/blob/master/transcripts/20190529T0900-r4ds-relational_data.R),<br />[Transcript strings](https://gitlab.com/omsai/teaching/blob/master/transcripts/20190529T1030-r4ds-strings_and_regex.R),<br />[Teaching notes](R-for-Data-Science.md),<br />[Student notes](https://pad.carpentries.org/2019-05-01-r-for-data-science-ct),<br />[Workshop](https://smcclatchy.github.io/2019-05-01-r-for-data-science-ct/)

## Imaging workshops

Date         | Topic                             | Location                  | Resources
------------:|-----------------------------------|---------------------------|----------
Feb 11, 2020 | Python Signal Processing - Helper | Jackson Labs              | [Git repo](https://github.com/TheJacksonLaboratory/images-as-signals),<br />[Workshop](https://smcclatchy.github.io/2020-02-13-python-signal-processing/)
Jan 31, 2019 | Python Imaging - Helper           | Jackson Labs              | [Notebook history](https://gitlab.com/omsai/pythonimagingbasic/blob/master/lessons/Untitled.ipynb),<br />[Student notes](https://pad.carpentries.org/2019-01-31-image-analysis-ct),<br />[Workshop](https://thejacksonlaboratory.github.io/2019-01-30-image-analysis-ct/)
Jan 17, 2019 | ImageJ - Helper                   | University of Connecticut | [Workshop](https://confocal.uconn.edu/about-us/)
Jun  7, 2017 | ImageJ - Helper                   | University of Connecticut | (same as above)
Jan 12, 2017 | ImageJ - Helper                   | University of Connecticut | (same as above)
