library(tidyverse)
library(stringr)

## 14.2 - String basics ----
string1 <- "This is a string"
string1

c("one", "two", "three")

## 14.2.1 - String length ----
str_length(c("a", "string of characters", NA))

## Challenge 1 ----
## What does str_trim() do?
?str_trim
example(str_trim)

## 14.2.2 - Combining strings ----
## Don't use paste()
str_c("x", "y")
str_c("x", "y", sep = ", ")

## Recycling.
str_c("prefix-", c("a", "b", "c"), "-suffix")
str_c("prefix-", c("a", "b", "c"), "-suffix", collapse = ", ")
?str_c

## Fixing NA
str_c("prefix-", c("a", "b", NA, "c"), "-suffix")
str_c("prefix-", str_replace_na(c("a", "b", NA, "c"),
                                replacement = ""), "-suffix")
?str_replace_na

## 14.2.3 - Subsetting strings ----
x <- c("aPple", "banana", "pear")
# Extract letters using str_sub.
str_sub(x, 1, 3)
x
str_sub(x, -3, -1)
str_sub(x, -1, -3)

## Challenge 2 ----
## Use str_length() and str_sub() to extract the middle 
## character from a string
middle <- str_length(x) / 2 + 0.5
middle
middle <- ceiling(middle)
middle
str_sub(x, middle, middle)

## 14.3 - Regular expressions -----
# Exact match.
str_view(x, "an")
# "." matches any character.
str_view(x, ".a.")
str_view(c("abc", "a.c"), "a.c")
# To match the literal dot ".", use slashes
str_view(c("abc", "a.c"), "a\\.c")
# R gobbles up the first slash.
writeLines("a\\.c")
writeLines("a\.c")

## Challenge 4 ----
## We saw the need for an extra slash to atch a literal dot "."
## in "a.c" Using "\.".
## How would you match the slash "\" in a\b?
x <- c("a\\b")
writeLines(x)
x
str_view(x, "\\\\")
writeLines("\\\\")
x

## 14.3.2 - Anchors ----
x <- c("apple", "banana", "pear")
x
str_view(x, "^a")
str_view(x, "a$")
y <- c("apple pie", "apple", "apple cake")
str_view(y, "^apple$")

## Challenge 5 ----
## Match words exactly 3 letters long.
words
str_view(words, "...", match = TRUE)
## Fix this above command!
str_view(words, "^...$", match = TRUE)

## 14.3.3 - Alternatives ---
y <- c("singing", "exercise", "riser")
# I only want the endings, "ing" or "ise".
str_view(y, "ing$")
str_view(y, "ise$")
str_view(y, "ing$|ise$")
str_view(y, "(ing|ise|er)$")
# Match alternative letters.
y <- c("grayscale", "grey")
str_view(y, "gr[ae]y")
str_view(y, "gray|grey")

## 14.3.4 - Repetition -----
y <- "1888 is the longest C year in CX Roman numerals: MDCCCLXXXVIII"
## + = 1 or more
## * = 0 or more
str_view(y, "C+")
str_view(y, "LX*")
str_view_all(y, "CX*")
str_view_all(words, "e", match = TRUE)
## {n} Exactly n repetitions
## {n,} n or more
## {,m} at most m
## {n,m} between n and m
str_view(y, "C{2}")
str_view(y, "C{2,}")
str_view(y, "C{2,3}")
str_view(y, "C{2,3}?") # ? makes it less "greedy"
str_view(y, "C[LX]{2}")

# Challenge 6 ----
# What does this expression match:
# ^.*$
# \d{3}-\d{2}-\d{4}

## 14.3.5 - Grouping and back references ----
fruit
str_view(fruit, "(..)\\1", match = TRUE)

## 14.4.1 - Detect Matches ----
x
str_detect(x, "e")
words
mean(str_detect(words, "[aeiou]"))
words[str_detect(words, "x$")]
df <- tibble(
  word = words,
  i = seq_along(word)
)
df
df %>% filter(str_detect(word, "x$"))

## Challenge 7---
## Find words that start with a vowel and end in a consonant.
## Hint: a, words without a can be matched with [^a]
str_view_all(x, "[^a]")
str_view(tail(words, 100), "^[aeiou].*[^aeiou]$", match = TRUE)

## 14.4.4 - Replacing matches ----
x
str_replace(x, "[aeiou]", "-")
str_replace_all(x, "[aeiou]", "-")
head(sentences, 3)
str_replace(head(sentences, 3),
            "([^ ]+) ([^ ]+) ([^ ]+)", "\\1 \\3 \\2")
my_sentences <- head(sentences, 3)
my_sentences
str_split(my_sentences, boundary("word"), n = 3, simplify = TRUE)
## Named replacements!
y <- c("1 2 3 house", "2 cars", "3 people")
str_replace_all(y, c("1" = "one", "2" = "two", "3" = "three"))
y
